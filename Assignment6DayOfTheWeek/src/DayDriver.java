import java.util.Scanner;

public class DayDriver {

	public static void main(String[] args) {
		
		int newDay;
		int addDays;
		
		System.out.println("Enter 0-6 for a day of the week.");
		Day dow = new Day();
		Scanner keyboard = new Scanner(System.in);
		newDay = keyboard.nextInt();
		dow.setDay(newDay);
		dow.writeOutput();
		System.out.println("The next day is: "+dow.getNextDay(newDay));
		System.out.println("The previous day is: "+dow.getPreviousDay(newDay));
		
		System.out.println("Enter the amount of days you would like to add to the current day:");
		addDays = keyboard.nextInt();
		System.out.println("The new day would be: " + dow.getFartherDays(newDay, addDays));

		
	}

}
