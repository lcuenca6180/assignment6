/**
 * Methods for use in the DayDriver class and DayDriverGUI class
 */
public class Day {
	
	private static String dia;
	private static int newDay;
	private static String newDia;
	String [] dayOfWeek={"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
/**
* Sets the day of the week from corresponding number.
* @param newDay will be a number from 0-6
*/
	public void setDay(int newDay)
	{
		dia = dayOfWeek[newDay];	
	}
/**
 * Gets the day stored.
 * @return dia, spanish for day
 */
	public String getDay()
	{
		return dia;
	}
/**
 * Calculates the day after the setDay
 * @param newDay
 * @return the day after the setDay
 */
	public String getNextDay(int newDay)
	{
		
		return dayOfWeek[(newDay +1)%7];
	}
/**
 * Calculates the day prior to the setDay
 * @param newDay
 * @return previous day
 */
	public String getPreviousDay(int newDay)
	{
		return dayOfWeek[((newDay+7)%7)-1];
	}
/**
 * Prints out the set day when invoked.
 */
	public void writeOutput()
	{
		System.out.println("The day of the week is: "+ getDay());
		//System.out.println("The next day would be: " + getNextDay(newDay));
		//System.out.println("The previous day would be: " + getPreviousDay(newDay));

	}
/**
 * Returns the setDay in the DayDriverGUI class
 * @return litterally the getDay
 */
	public String writeOutput2()
	{
		return getDay();
	}
/**
 * Adds any amount to the setDay to give you the newest day of the week.
 * @param newDay
 * @param addDays
 * @return Any day of the week that the user decides to add.
 */
	public String getFartherDays(int newDay, int addDays)
	{
		//newDia = dayOfWeek[(newDay+addDays)%7];
		return dayOfWeek[(newDay+addDays)%7];
	}
	
	

}
