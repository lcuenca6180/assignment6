import javax.swing.JOptionPane;

public class DayDriverGUI {

	public static void main(String[] args) {
		int newDay;
		int addDays;
		
		String prompt = JOptionPane.showInputDialog("Enter 0-6 for a day of the week.") ;
		Day dow = new Day();
		newDay = Integer.parseInt(prompt);
		dow.setDay(newDay);
		
		JOptionPane.showMessageDialog(null,"The day you picked is: " + dow.writeOutput2());
		JOptionPane.showMessageDialog(null,"The next day is: " +dow.getNextDay(newDay));
		JOptionPane.showMessageDialog(null,"The previous day is: " + dow.getPreviousDay(newDay));
		
		String prompt2 = JOptionPane.showInputDialog("How many days would you like to add to the initial day?") ;
		addDays = Integer.parseInt(prompt2);
		
		JOptionPane.showMessageDialog(null,"The new day is: " + dow.getFartherDays(newDay, addDays));
		
		System.exit(0);
	}

}
